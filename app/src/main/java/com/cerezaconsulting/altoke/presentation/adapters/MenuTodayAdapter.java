package com.cerezaconsulting.altoke.presentation.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.presentation.adapters.listeners.OnClickListListener;
import com.cerezaconsulting.altoke.presentation.utils.AnimationViewUtils;
import com.cerezaconsulting.altoke.presentation.utils.DayUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 17/04/17.
 */

public class MenuTodayAdapter extends RecyclerView.Adapter<MenuTodayAdapter.ViewHolder> implements OnClickListListener {

    private ArrayList<FoodEntity> list;
    private Context context;
    private ArrayList<Boolean> status;

    public MenuTodayAdapter(ArrayList<FoodEntity> list, Context context) {
        this.context = context;
        this.list = list;
        setStatus();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(root, this);
    }

    private void setStatus() {
        status = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            status.add(false);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FoodEntity foodEntity = list.get(position);
        holder.tvMenuDish.setText(foodEntity.getMain_dish());
        holder.tvMenuBeverage.setText(foodEntity.getDrink());
        holder.tvMenuEntry.setText(foodEntity.getEntry());
        holder.tvTitleCombo.setText(foodEntity.getName_lunch());
        String onlyXDishesLeft = context.getResources().getString(R.string.only_left) + " " + foodEntity.getStock() + " platos";
        holder.tvOnlyXDishesLeft.setText(onlyXDishesLeft);
        holder.tvMenuDate.setText(DayUtils.getCalendarStringMenu());
        paintHolder(holder, position);
    }

    private void paintHolder(ViewHolder holder, int position) {
        AnimationViewUtils.fadeView(holder.ivCheck,!status.get(position));
        holder.frBackgroundTitle.setBackgroundColor(status.get(position) ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
        holder.frItemMenu.setBackground(status.get(position) ? context.getResources().getDrawable(R.drawable.background_border_dark) : null);
        holder.tvMenuEntry.setTextColor(status.get(position) ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
        holder.tvMenuDish.setTextColor(status.get(position) ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
        holder.tvMenuBeverage.setTextColor(status.get(position) ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
    }

    public void setList(ArrayList<FoodEntity> list) {
        this.list = list;
        setStatus();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public FoodEntity getItemSelected() {
        FoodEntity foodEntity = null;
        for (int i = 0; i < status.size(); i++) {
            if (status.get(i)) {
                foodEntity = list.get(i);
                i = status.size();
            }
        }
        return foodEntity;
    }

    @Override
    public void onClick(int position) {
        for (int i = 0; i < status.size(); i++) {
            if (position == i) {
                status.set(i, !status.get(i));
            } else {
                status.set(i, false);
            }
        }
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_menu_date)
        TextView tvMenuDate;
        @BindView(R.id.tv_title_combo)
        TextView tvTitleCombo;
        @BindView(R.id.fr_background_title)
        FrameLayout frBackgroundTitle;
        @BindView(R.id.tv_menu_entry)
        TextView tvMenuEntry;
        @BindView(R.id.tv_menu_dish)
        TextView tvMenuDish;
        @BindView(R.id.tv_menu_beverage)
        TextView tvMenuBeverage;
        @BindView(R.id.fr_item_menu)
        LinearLayout frItemMenu;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.iv_check)
        ImageView ivCheck;
        @BindView(R.id.tv_only_x_dishes_left)
        TextView tvOnlyXDishesLeft;
        @BindView(R.id.ll_select_delivery_point)
        LinearLayout llSelectDeliveryPoint;
        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
