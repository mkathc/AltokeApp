package com.cerezaconsulting.altoke.presentation.contracts;

import com.cerezaconsulting.altoke.core.BasePresenter;
import com.cerezaconsulting.altoke.core.BaseView;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;

import java.util.ArrayList;

/**
 * Created by miguel on 17/04/17.
 */

public interface MenuTodayContract {
    interface View extends BaseView<Presenter>{
        void loadFood(ArrayList<FoodEntity> list);
        void reserveFoodSuccessfully();
    }
    interface Presenter extends BasePresenter{
        void createReservation(String idFood,String quantity);
    }
}
