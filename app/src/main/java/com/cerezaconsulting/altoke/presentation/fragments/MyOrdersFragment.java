package com.cerezaconsulting.altoke.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.core.BaseFragment;
import com.cerezaconsulting.altoke.core.ScrollChildSwipeRefreshLayout;
import com.cerezaconsulting.altoke.data.entities.ReservationEntity;
import com.cerezaconsulting.altoke.presentation.adapters.MyOrdersAdapter;
import com.cerezaconsulting.altoke.presentation.contracts.MyOrdersContract;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 24/02/17.
 */

public class MyOrdersFragment extends BaseFragment implements MyOrdersContract.View{


    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.refresh_layout)
    ScrollChildSwipeRefreshLayout refreshLayout;

    private MyOrdersContract.Presenter presenter;
    private LinearLayoutManager layoutManager;
    private MyOrdersAdapter adapter;


    public static MyOrdersFragment newInstance() {
        return new MyOrdersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_orders, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.black),
                ContextCompat.getColor(getActivity(), R.color.dark_gray),
                ContextCompat.getColor(getActivity(), R.color.black)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        refreshLayout.setScrollUpChild(rvList);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.start();
            }
        });
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(layoutManager);
        adapter = new MyOrdersAdapter(new ArrayList<ReservationEntity>(),getContext());
        rvList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void getMyOrders(ArrayList<ReservationEntity> list) {
        if(adapter!=null){
            adapter.setItems(list);

        }
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }

        // Make sure setRefreshing() is called after the layout is done with everything else.
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(active);
            }
        });
    }

    @Override
    public void setMessageError(String error) {
        ((BaseActivity) getActivity()).showMessageError(error);
    }

    @Override
    public void setDialogMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MyOrdersContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
