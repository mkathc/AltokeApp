package com.cerezaconsulting.altoke.presentation.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 9/05/17.
 */

public class ContactUsFragment extends BaseFragment {

    @BindView(R.id.btn_call)
    Button btnCall;
    @BindView(R.id.btn_email)
    Button btnEmail;
    @BindView(R.id.btn_message)
    Button btnMessage;

    public static ContactUsFragment newInstance() {
        return new ContactUsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @OnClick({R.id.btn_call, R.id.btn_email,R.id.btn_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+51935149724"));
                startActivity(intent);
                break;
            case R.id.btn_email:
                Intent itSend = new Intent(Intent.ACTION_SEND);
                //vamos a enviar texto plano
                itSend.setType("plain/text");
                //colocamos los datos para el envío
                itSend.putExtra(Intent.EXTRA_EMAIL, new String[]{"altokeappteam@gmail.com"});
                startActivity(itSend);
                break;
            case R.id.btn_message:
                Uri uri = Uri.parse("smsto:+51935149724");
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, ""));
                break;
        }
    }

}
