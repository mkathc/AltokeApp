package com.cerezaconsulting.altoke.presentation.contracts;

import com.cerezaconsulting.altoke.core.BasePresenter;
import com.cerezaconsulting.altoke.core.BaseView;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.cerezaconsulting.altoke.data.entities.enums.TypeLoginEnum;

/**
 * Created by miguel on 10/04/17.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter>{
        void loginSuccessfully(UserEntity userEntity, TypeLoginEnum typeLogin);
    }
    interface Presenter extends BasePresenter{
        void loginFacebook(String token);
        void loginGmail(String token);
    }
}
