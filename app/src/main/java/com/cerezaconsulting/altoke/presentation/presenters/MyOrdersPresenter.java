package com.cerezaconsulting.altoke.presentation.presenters;

import android.content.Context;

import com.cerezaconsulting.altoke.data.entities.ReservationEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.UserRequest;
import com.cerezaconsulting.altoke.presentation.contracts.MyOrdersContract;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 19/04/17.
 */

public class MyOrdersPresenter implements MyOrdersContract.Presenter {

    private MyOrdersContract.View mView;
    private SessionManager sessionManager;

    public MyOrdersPresenter(MyOrdersContract.View mView, Context context) {
        this.mView = mView;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.setLoadingIndicator(true);
        String token = sessionManager.getUserToken();
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<TrackEntityHolder<ReservationEntity>> call = userRequest.getMyReservations("Token "+token);
        call.enqueue(new Callback<TrackEntityHolder<ReservationEntity>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<ReservationEntity>> call, Response<TrackEntityHolder<ReservationEntity>> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    mView.getMyOrders(response.body().getResults());
                }
                else{
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<ReservationEntity>> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }
}
