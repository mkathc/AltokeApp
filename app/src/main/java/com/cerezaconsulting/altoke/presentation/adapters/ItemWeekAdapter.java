package com.cerezaconsulting.altoke.presentation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.presentation.contracts.MenuWeekContract;
import com.cerezaconsulting.altoke.presentation.utils.AnimationViewUtils;
import com.cerezaconsulting.altoke.presentation.utils.DayUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 20/04/17.
 */

public class ItemWeekAdapter extends RecyclerView.Adapter<ItemWeekAdapter.ViewHolder> {

    private ArrayList<FoodEntity> list;
    private Date date;
    private Context context;
    private SessionManager sessionManager;
    private MenuWeekContract.Presenter mView;

    public ItemWeekAdapter(ArrayList<FoodEntity> list, Context context, Date date, MenuWeekContract.Presenter mView) {
        this.list = list;
        this.date = date;
        this.context = context;
        sessionManager = new SessionManager(context);
        this.mView = mView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_week_viewpager, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final FoodEntity foodEntity = list.get(position);
        holder.tvMenuDate.setText(DayUtils.getCalendarString(date));
        holder.tvTitleCombo.setText(foodEntity.getName_lunch());
        holder.tvMenuBeverage.setText(foodEntity.getDrink());
        holder.tvMenuEntry.setText(foodEntity.getEntry());
        holder.tvMenuDish.setText(foodEntity.getMain_dish());
        String onlyXDishesLeft = context.getResources().getString(R.string.only_left) + " " + foodEntity.getStock() + " platos";
        holder.tvOnlyXDishesLeft.setText(onlyXDishesLeft);
        /*holder.cbReserveCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuantityDialog(foodEntity,holder);
            }
        });
        holder.llReserveCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuantityDialog(foodEntity,holder);
            }
        });*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuantityDialog(list.get(holder.getAdapterPosition()),holder);
            }
        });
    }

    private void paintHolder(ViewHolder holder, boolean status) {
        AnimationViewUtils.fadeView(holder.ivCheck, !status);
        holder.frBackgroundTitle.setBackgroundColor(status ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
        holder.frItemMenu.setBackground(status ? context.getResources().getDrawable(R.drawable.background_border_dark) : null);
        holder.tvMenuEntry.setTextColor(status ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
        holder.tvMenuDish.setTextColor(status ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
        holder.tvMenuBeverage.setTextColor(status ? context.getResources().getColor(R.color.gray) : context.getResources().getColor(R.color.colorAccent));
    }

    private void showQuantityDialog(final FoodEntity foodEntity, final ViewHolder holder) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        final int day = calendar.get(Calendar.DAY_OF_WEEK);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reserve_menu);
        Button button = (Button) dialog.findViewById(R.id.btn_accept);
        final TextView textView = (TextView) dialog.findViewById(R.id.tv_stock);
        String text = "Stock: "+foodEntity.getStock();
        textView.setText(text);
        final EditText editText = (EditText) dialog.findViewById(R.id.et_quantity);
        ImageView close = (ImageView) dialog.findViewById(R.id.iv_close);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quantity = editText.getText().toString();
                if (!quantity.isEmpty()) {
                    int q = Integer.parseInt(quantity);
                    if (q > 0 && q <= foodEntity.getStock()) {
                        if(sessionManager.isLogin()) {
                            paintHolder(holder, true);
                            mView.createReservationByDate(date, foodEntity, quantity);
                            mView.updateTabPosition(day);
                            dialog.dismiss();
                        }
                        else{
                            mView.sendError(context.getString(R.string.please_login_to_reserve));
                        }
                    } else {
                        mView.sendError(context.getString(R.string.please_enter_a_valid_quantity));
                    }
                } else {
                    mView.sendError(context.getString(R.string.please_enter_a_valid_quantity));
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                holder.cbReserveCombo.setChecked(false);
            }
        });
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_menu_date)
        TextView tvMenuDate;
        @BindView(R.id.tv_title_combo)
        TextView tvTitleCombo;
        @BindView(R.id.fr_background_title)
        FrameLayout frBackgroundTitle;
        @BindView(R.id.tv_menu_entry)
        TextView tvMenuEntry;
        @BindView(R.id.tv_menu_dish)
        TextView tvMenuDish;
        @BindView(R.id.tv_menu_beverage)
        TextView tvMenuBeverage;
        @BindView(R.id.ll_description_menu)
        LinearLayout llDescriptionMenu;
        @BindView(R.id.fr_item_menu)
        LinearLayout frItemMenu;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.iv_check)
        ImageView ivCheck;
        @BindView(R.id.cb_reserve_combo)
        AppCompatCheckBox cbReserveCombo;
        @BindView(R.id.tv_reserve_combo)
        TextView tvReserveCombo;
        @BindView(R.id.tv_only_x_dishes_left)
        TextView tvOnlyXDishesLeft;
        @BindView(R.id.ll_reserve_combo)
        LinearLayout llReserveCombo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
