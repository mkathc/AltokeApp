package com.cerezaconsulting.altoke.presentation.presenters;

import android.content.Context;
import android.util.Log;

import com.cerezaconsulting.altoke.data.entities.DateEntity;
import com.cerezaconsulting.altoke.data.entities.FoodByDayEntity;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.entities.ReservationEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.FoodRequest;
import com.cerezaconsulting.altoke.data.repositories.remote.request.UserRequest;
import com.cerezaconsulting.altoke.presentation.contracts.MenuWeekContract;
import com.cerezaconsulting.altoke.presentation.utils.DayUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 20/04/17.
 */

public class MenuWeekPresenter implements MenuWeekContract.Presenter {

    private MenuWeekContract.View mView;
    private SessionManager sessionManager;

    public MenuWeekPresenter(MenuWeekContract.View mView, Context context) {
        this.mView = mView;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.setLoadingIndicator(true);
        String days = "";
        PlacesEntity placesEntity = sessionManager.getPlace();
        FoodRequest foodRequest = ServiceFactory.createService(FoodRequest.class);
        Call<TrackEntityHolder<FoodEntity>> call = foodRequest.getFoodByPlace(placesEntity.getId(),days);
        call.enqueue(new Callback<TrackEntityHolder<FoodEntity>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<FoodEntity>> call, Response<TrackEntityHolder<FoodEntity>> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    orderByDay(response.body().getResults());
                }
                else{
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<FoodEntity>> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }

    private void orderByDay(ArrayList<FoodEntity> list){
        ArrayList<FoodByDayEntity>  foodByDayEntities = new ArrayList<>();
        foodByDayEntities.add(new FoodByDayEntity("lunes"));
        foodByDayEntities.add(new FoodByDayEntity("martes"));
        foodByDayEntities.add(new FoodByDayEntity("miercoles"));
        foodByDayEntities.add(new FoodByDayEntity("jueves"));
        foodByDayEntities.add(new FoodByDayEntity("viernes"));
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i).getDay()){
                case "lunes":
                    foodByDayEntities.get(0).getFoodEntities().add(list.get(i));
                    break;
                case "martes":
                    foodByDayEntities.get(1).getFoodEntities().add(list.get(i));
                    break;
                case "miercoles":
                    foodByDayEntities.get(2).getFoodEntities().add(list.get(i));
                    break;
                case "jueves":
                    foodByDayEntities.get(3).getFoodEntities().add(list.get(i));
                    break;
                case "viernes":
                    foodByDayEntities.get(4).getFoodEntities().add(list.get(i));
                    break;
                default:
                    break;
            }
        }
        addDates(foodByDayEntities);
    }

    private void addDates(ArrayList<FoodByDayEntity> list){
        ArrayList<DateEntity> dateEntities = DayUtils.getDates();
        for (int d = 0; d < dateEntities.size(); d++) {
            for (int i = 0; i < list.size(); i++) {
                if(list.get(i).getDay().equals(dateEntities.get(d).getDay())){
                    list.get(i).setDate(dateEntities.get(d).getDate());
                }
            }
        }

        mView.loadDays(list);

    }

    @Override
    public void sendError(String message) {
        mView.setMessageError(message);
    }

    @Override
    public void updateTabPosition(int position) {
        mView.getTabPositionChange(position);
    }

    @Override
    public void createReservationByDate(Date date, FoodEntity foodEntity, String quantity) {
        mView.setLoadingIndicator(true);
        String token = sessionManager.getUserToken();
        PlacesEntity placesEntity = sessionManager.getPlace();
        String d = DayUtils.getActualDate(date);
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<ReservationEntity> call = userRequest.createReservation("Token "+token,foodEntity.getId(),placesEntity.getId(),d,quantity,true);
        call.enqueue(new Callback<ReservationEntity>() {
            @Override
            public void onResponse(Call<ReservationEntity> call, Response<ReservationEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    mView.reserveFoodSuccessfully();
                }
                else{
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<ReservationEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }
}
