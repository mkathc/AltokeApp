package com.cerezaconsulting.altoke.presentation.presenters;

import android.content.Context;
import android.util.Log;

import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.entities.ReservationEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.FoodRequest;
import com.cerezaconsulting.altoke.data.repositories.remote.request.UserRequest;
import com.cerezaconsulting.altoke.presentation.contracts.MenuTodayContract;
import com.cerezaconsulting.altoke.presentation.utils.DayUtils;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 17/04/17.
 */

public class MenuTodayPresenter implements MenuTodayContract.Presenter {

    private MenuTodayContract.View mView;
    private SessionManager sessionManager;

    public MenuTodayPresenter(MenuTodayContract.View mView, Context context) {
        this.mView = mView;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.setLoadingIndicator(true);
        PlacesEntity placesEntity = sessionManager.getPlace();
        String dayToday = DayUtils.getDayString();
        FoodRequest foodRequest = ServiceFactory.createService(FoodRequest.class);
        Call<TrackEntityHolder<FoodEntity>> call = foodRequest.getFoodByPlace(placesEntity.getId(),dayToday);
        call.enqueue(new Callback<TrackEntityHolder<FoodEntity>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<FoodEntity>> call, Response<TrackEntityHolder<FoodEntity>> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    mView.loadFood(response.body().getResults());
                }
                else{
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<FoodEntity>> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se puede conectar al servidor, por favor intente más tarde");
            }
        });
    }

    @Override
    public void createReservation(String idFood, String quantity) {
        mView.setLoadingIndicator(true);
        String token = sessionManager.getUserToken();
        PlacesEntity placesEntity = sessionManager.getPlace();
        String date = DayUtils.getActualDate();
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<ReservationEntity> call = userRequest.createReservation("Token "+token,idFood,placesEntity.getId(),date,quantity,true);
        call.enqueue(new Callback<ReservationEntity>() {
            @Override
            public void onResponse(Call<ReservationEntity> call, Response<ReservationEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    mView.reserveFoodSuccessfully();
                }
                else{
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<ReservationEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }
}
