package com.cerezaconsulting.altoke.presentation.utils;

import com.cerezaconsulting.altoke.data.entities.DateEntity;
import com.cerezaconsulting.altoke.data.entities.enums.DaysEnum;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by miguel on 17/04/17.
 */

public class DayUtils {
    public static String getDayString(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String dayToday;
        switch (day) {
            case Calendar.SUNDAY:
                dayToday = "lunes";
                break;
            case Calendar.FRIDAY:
                dayToday = "viernes";
                break;
            case Calendar.MONDAY:
                dayToday = "lunes";
                break;
            case Calendar.SATURDAY:
                dayToday = "lunes";
                break;
            case Calendar.THURSDAY:
                dayToday = "jueves";
                break;
            case Calendar.TUESDAY:
                dayToday = "martes";
                break;
            case Calendar.WEDNESDAY:
                dayToday = "miercoles";
                break;
            default:
                dayToday = "";
                break;
        }
        return dayToday;
    }

    public static String getActualDate(Date date){
        String d;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",new Locale("es","ES"));
        d= dateFormat.format(date);
        return d;
    }

    public static String getActualDate(){
        String date;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",new Locale("es","ES"));
        Date d = new Date();
        date = dateFormat.format(d);
        return date;
    }

    public static String getCalendarString(String date){
        String actualDate="";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd",new Locale("es","ES"));
        DateFormat dateFormat = new SimpleDateFormat("dd' de 'MMMM' del 'yyyy",new Locale("es","ES"));
        try {
            Date d = format.parse(date);
            actualDate = getDayOfDate(d);
            actualDate = actualDate+", ";
            actualDate = actualDate+dateFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return actualDate;
    }

    public static String getCalendarString(Date date){
        String actualDate;
        DateFormat dateFormat = new SimpleDateFormat("dd' de 'MMMM' del 'yyyy",new Locale("es","ES"));
        actualDate = getDayOfDate(date);
        actualDate = actualDate+", ";
        actualDate = actualDate+dateFormat.format(date);
        return actualDate;
    }

    private static String getDayOfDate(Date d){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String dayToday;
        switch (day) {
            case Calendar.SUNDAY:
                dayToday = "Domingo";
                break;
            case Calendar.FRIDAY:
                dayToday = "Viernes";
                break;
            case Calendar.MONDAY:
                dayToday = "Lunes";
                break;
            case Calendar.SATURDAY:
                dayToday = "Sábado";
                break;
            case Calendar.THURSDAY:
                dayToday = "Jueves";
                break;
            case Calendar.TUESDAY:
                dayToday = "Martes";
                break;
            case Calendar.WEDNESDAY:
                dayToday = "Miércoles";
                break;
            default:
                dayToday = "";
                break;
        }
        return dayToday;
    }

    public static String getCalendarStringMenu(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String dayToday;
        switch (day) {
            case Calendar.SUNDAY:
                dayToday = "Lunes";
                dayToday = dayToday+getDateParser(DayOfWeek.sunday);
                break;
            case Calendar.FRIDAY:
                dayToday = "Viernes";
                dayToday = dayToday+getDateParser(DayOfWeek.weekday);
                break;
            case Calendar.MONDAY:
                dayToday = "Lunes";
                dayToday = dayToday+getDateParser(DayOfWeek.weekday);
                break;
            case Calendar.SATURDAY:
                dayToday = "Lunes";
                dayToday = dayToday+getDateParser(DayOfWeek.saturday);
                break;
            case Calendar.THURSDAY:
                dayToday = "Jueves";
                dayToday = dayToday+getDateParser(DayOfWeek.weekday);
                break;
            case Calendar.TUESDAY:
                dayToday = "Martes";
                dayToday = dayToday+getDateParser(DayOfWeek.weekday);
                break;
            case Calendar.WEDNESDAY:
                dayToday = "Miércoles";
                dayToday = dayToday+getDateParser(DayOfWeek.weekday);
                break;
            default:
                dayToday = "";
                break;
        }
        return dayToday;
    }

    private static String getDateParser(DayOfWeek day){
        String date = ", ";
        Date d = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd' de 'MMMM' del 'yyyy",new Locale("es","ES"));
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        switch (day){
            case weekday:
                date=date+dateFormat.format(d);
                break;
            case saturday:
                c.add(Calendar.DATE, 2);
                d = c.getTime();
                date=date+dateFormat.format(d);
                break;
            case sunday:
                c.add(Calendar.DATE, 1);
                d = c.getTime();
                date=date+dateFormat.format(d);
                break;
        }
        return date;
    }

    public static ArrayList<DateEntity> getDates(){
        ArrayList<DateEntity> dateEntities = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Date date = calendar.getTime();
        switch (day) {
            case Calendar.FRIDAY:
                dateEntities.add(new DateEntity("viernes",date));
                calendar.add(Calendar.DATE,3);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("lunes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("martes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("miercoles",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("jueves",date));
                break;
            case Calendar.MONDAY:
                dateEntities.add(new DateEntity("lunes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("martes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("miercoles",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("jueves",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("viernes",date));
                break;
            case Calendar.SATURDAY:
                calendar.add(Calendar.DATE,2);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("lunes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("martes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("miercoles",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("jueves",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("viernes",date));
                break;
            case Calendar.SUNDAY:
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("lunes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("martes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("miercoles",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("jueves",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("viernes",date));
                break;
            case Calendar.THURSDAY:
                dateEntities.add(new DateEntity("jueves",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("viernes",date));
                calendar.add(Calendar.DATE,3);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("lunes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("martes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("miercoles",date));
                break;
            case Calendar.TUESDAY:
                dateEntities.add(new DateEntity("martes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("miercoles",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("jueves",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("viernes",date));
                calendar.add(Calendar.DATE,3);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("lunes",date));
                break;
            case Calendar.WEDNESDAY:
                dateEntities.add(new DateEntity("miercoles",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("jueves",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("viernes",date));
                calendar.add(Calendar.DATE,3);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("lunes",date));
                calendar.add(Calendar.DATE,1);
                date = calendar.getTime();
                dateEntities.add(new DateEntity("martes",date));
                break;
        }
        return dateEntities;
    }

    private enum DayOfWeek{
        weekday,
        saturday,
        sunday
    }
}
