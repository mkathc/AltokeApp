package com.cerezaconsulting.altoke.presentation.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.presentation.adapters.listeners.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 23/02/17.
 */

public class SelectUniversityAdapter extends RecyclerView.Adapter<SelectUniversityAdapter.ViewHolder> implements OnClickListListener {

    private ArrayList<PlacesEntity> list;
    private ArrayList<Boolean> state;
    private Context context;

    public SelectUniversityAdapter(ArrayList<PlacesEntity> list, Context context) {
        this.list = list;
        setupSelect();
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_university, parent, false);
        return new ViewHolder(root, this);
    }

    public void setItems(ArrayList<PlacesEntity> list) {
        this.list = list;
        setupSelect();
        notifyDataSetChanged();
    }

    public void setItems(ArrayList<PlacesEntity> list,PlacesEntity placesEntity) {
        this.list = list;
        setupSelect();
        selectPlace(placesEntity);
        notifyDataSetChanged();
    }

    private void selectPlace(PlacesEntity placesEntity){
        for (int i = 0; i < list.size(); i++) {
            if(placesEntity.getId().equals(list.get(i).getId())){
                state.set(i,true);
                return;
            }
        }
    }

    public PlacesEntity getItemSelected() {
        PlacesEntity temp = null;
        for (int i = 0; i < state.size(); i++) {
            if (state.get(i)) {
                temp = list.get(i);
            }
        }
        return temp;
    }

    private void setupSelect() {
        state = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            state.add(false);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PlacesEntity universityEntity = list.get(position);
        holder.tvUniversity.setText(universityEntity.getName());
        if (universityEntity.getImage() != null) {
            Glide.with(context).load(universityEntity.getImage()).into(holder.ivUniversity);
        }
        if(state.get(position)){
            holder.llUniversity.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.background_default));
        }
        else{
            holder.llUniversity.setBackgroundDrawable(null);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(int position) {
        for (int i = 0; i < list.size(); i++) {
            if(i==position) {
                state.set(i, !state.get(i));
            }
            else{
                state.set(i,false);
            }

        }
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ll_university)
        LinearLayout llUniversity;
        @BindView(R.id.iv_university)
        ImageView ivUniversity;
        @BindView(R.id.tv_university)
        TextView tvUniversity;
        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
