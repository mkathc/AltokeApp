package com.cerezaconsulting.altoke.presentation.presenters;

import android.content.Context;

import com.cerezaconsulting.altoke.data.entities.AccessTokenEntity;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.cerezaconsulting.altoke.data.entities.enums.TypeLoginEnum;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.LoginRequest;
import com.cerezaconsulting.altoke.data.repositories.remote.request.UserRequest;
import com.cerezaconsulting.altoke.presentation.contracts.LoginContract;
import com.facebook.AccessToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 10/04/17.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View mView;
    private SessionManager sessionManager;
    private static final String URL_PHOTO_GMAIL= "http://picasaweb.google.com/data/entry/api/user/";
    private TypeLoginEnum typeLogin = TypeLoginEnum.FACEBOOK;

    public LoginPresenter(LoginContract.View mView, Context context) {
        this.mView = mView;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void loginFacebook(String token) {
        typeLogin = TypeLoginEnum.FACEBOOK;
        mView.setLoadingIndicator(true);
        LoginRequest loginRequest = ServiceFactory.createService(LoginRequest.class);
        Call<AccessTokenEntity> call = loginRequest.loginFacebook(token);
        call.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if(response.isSuccessful()){
                    getAccount(response.body());
                }
                else{
                    mView.setLoadingIndicator(false);
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo tener acceso al servidor, por favor intente más tarde");
            }
        });
    }

    private void getAccount(final AccessTokenEntity accessTokenEntity){
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<UserEntity> call = userRequest.getUser("Token "+accessTokenEntity.getAccessToken());
        call.enqueue(new Callback<UserEntity>() {
            @Override
            public void onResponse(Call<UserEntity> call, Response<UserEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if(response.isSuccessful()){
                    openSession(accessTokenEntity.getAccessToken(),response.body());
                }
                else{
                    mView.setLoadingIndicator(false);
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<UserEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo tener acceso al servidor, por favor intente más tarde");
            }
        });
    }

    private void openSession(String token,UserEntity userEntity){
        mView.setLoadingIndicator(false);
        sessionManager.openSession(token,userEntity);
        mView.loginSuccessfully(userEntity,typeLogin);
    }

    @Override
    public void loginGmail(final String token) {
        typeLogin = TypeLoginEnum.GMAIL;
        LoginRequest loginService =
                ServiceFactory.createService(LoginRequest.class);

        Call<AccessTokenEntity> call = loginService.loginGmail(token);
        mView.setLoadingIndicator(true);
        call.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }
                if(response.isSuccessful()){
                    getAccount(response.body());
                }
                else {
                    mView.setLoadingIndicator(false);
                    AccessToken.setCurrentAccessToken(null);
                    mView.setMessageError("No se pudo iniciar sesión, por favor inténtelo nuevamente");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }

                mView.setLoadingIndicator(false);
                AccessToken.setCurrentAccessToken(null);
                mView.setMessageError("Ocurrió un error al inicar sesión, por favor inténtelo nuevamente");
            }
        });
    }

    @Override
    public void start() {

    }
}
