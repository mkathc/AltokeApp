package com.cerezaconsulting.altoke.presentation.contracts;

import com.cerezaconsulting.altoke.core.BasePresenter;
import com.cerezaconsulting.altoke.core.BaseView;
import com.cerezaconsulting.altoke.data.entities.FoodByDayEntity;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by miguel on 20/02/17.
 */

public interface MenuWeekContract {
    interface View extends BaseView<Presenter>{
        void loadDays(ArrayList<FoodByDayEntity> list);
        void getTabPositionChange(int position);
        void reserveFoodSuccessfully();
    }
    interface Presenter extends BasePresenter{
        void sendError(String message);
        void updateTabPosition(int position);
        void createReservationByDate(Date date, FoodEntity foodEntity, String quantity);
    }
}
