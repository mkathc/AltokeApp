package com.cerezaconsulting.altoke.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 18/04/17.
 */

public class ReservationEntity implements Serializable {
    private String id;
    private String lunch;
    private String place_desired;
    private String lunch_name;
    private String day_reservation;
    private int quantity;
    private String maindish_name;
    private String entry_name;
    private String drink_name;
    private String place_name;
    private boolean is_enabled;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getPlace_desired() {
        return place_desired;
    }

    public void setPlace_desired(String place_desired) {
        this.place_desired = place_desired;
    }

    public String getDay_reservation() {
        return day_reservation;
    }

    public void setDay_reservation(String day_reservation) {
        this.day_reservation = day_reservation;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean is_enabled() {
        return is_enabled;
    }

    public void setIs_enabled(boolean is_enabled) {
        this.is_enabled = is_enabled;
    }

    public String getMaindish_name() {
        return maindish_name;
    }

    public void setMaindish_name(String maindish_name) {
        this.maindish_name = maindish_name;
    }

    public String getEntry_name() {
        return entry_name;
    }

    public void setEntry_name(String entry_name) {
        this.entry_name = entry_name;
    }

    public String getDrinks_name() {
        return drink_name;
    }

    public void setDrinks_name(String drinks_name) {
        this.drink_name = drinks_name;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public String getName_lunch() {
        return lunch_name;
    }

    public void setName_lunch(String name_lunch) {
        this.lunch_name = name_lunch;
    }
}
