package com.cerezaconsulting.altoke.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 23/02/17.
 */

public class UserEntity implements Serializable {
    private String id;
    private String email;
    private String first_name;
    private String last_name;
    private String cellphone;
    private String mode_user;
    private String birth_date;
    private String photo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getMode_user() {
        return mode_user;
    }

    public void setMode_user(String mode_user) {
        this.mode_user = mode_user;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFullName(){
        String fullName=null;
        if(first_name!=null && last_name!=null){
            fullName=first_name+" "+last_name;
        }
        return fullName;
    }
}
