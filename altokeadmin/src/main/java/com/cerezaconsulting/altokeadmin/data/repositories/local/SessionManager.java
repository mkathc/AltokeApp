package com.cerezaconsulting.altokeadmin.data.repositories.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.cerezaconsulting.altokeadmin.data.entities.UserEntity;
import com.google.gson.Gson;

/**
 * Created by miguel on 22/02/17.
 */

public class SessionManager {
    private static final String PREFERENCE_NAME = "ckeck_in";
    private static int PRIVATE_MODE = 0;

    /**
     USER DATA SESSION - JSON
     */

    private static final String USER_JSON = "user";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String IS_LOGIN = "is_login";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREFERENCE_NAME,PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void openSession(String token, UserEntity userEntity){
        editor.putString(ACCESS_TOKEN,token);
        editor.putBoolean(IS_LOGIN,true);
        String user = new Gson().toJson(userEntity);
        editor.putString(USER_JSON,user);
        editor.commit();
    }

    public boolean isLogin(){
        return preferences.getBoolean(IS_LOGIN,false);
    }
}
